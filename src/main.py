from keboola import docker
import logging_gelf.handlers
import logging_gelf.formatters
import xlrd  # noqa
import pandas as pd
import json
import csv  # noqa
import logging
import os
import sys  # noqa
"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_multi_xlsx_to_csv'"

"""
Python 3 environment 
"""

# import pip
# pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'logging_gelf'])


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)s - %(message)s')

logger = logging.getLogger('gelf')
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
)
logging_gelf_handler.setFormatter(
    logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])


# Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
# Adding XLSX filename
if "addFileName" in params:
    addFileName = bool(params["addFileName"])
else:
    addFileName = False
# Select sheets to parse
if "selectSheets" in params:
    selectSheets = params["selectSheets"]
else:
    selectSheets = []
# Sheets to ignore
if "ignoreSheets" in params:
    ignoreSheets = params["ignoreSheets"]
else:
    ignoreSheets = []

# Get proper list of tables
cfg = docker.Config('/data/')
in_files = cfg.get_input_files()
in_tables = cfg.get_input_tables()
out_files = cfg.get_expected_output_files()
out_tables = cfg.get_expected_output_tables()
logging.info(os.environ.get('KBC_DATADIR'))
logging.info("IN files mapped: {0}".format(in_files))
logging.info("IN tables mapped: {0}".format(in_tables))
logging.info("OUT files mapped: {0}".format(out_files))
logging.info("OUT tables mapped: {0}".format(out_tables))

# destination to fetch and output files
DEFAULT_TABLE_INPUT = "/data/in/tables/"
DEFAULT_TABLE_DESTINATION = "/data/out/tables/"
DEFAULT_FILE_INPUT = "/data/in/files/"
DEFAULT_FILE_DESTINATION = "/data/out/files/"


def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Returning list of input tables found.
    """

    in_name_list = []
    in_destination_list = []
    if len(in_tables) == 0:
        return in_name_list, in_destination_list

    # Input Files
    for table in in_tables:
        in_name = table["full_path"]
        in_destination = table["destination"]
        in_name_list.append(in_name)
        in_destination_list.append(in_destination)

    """logging.info("Data table: {0}".format(in_name_list))
    logging.info("Input table source: {0}".format(in_destination_list))"""

    return in_name_list, in_destination_list


def filter_sheets(available_sheets, selectSheets, ignoreSheets):
    '''
    Filtering in/out interested sheets
    '''

    filtered_sheets = []

    if selectSheets:
        '''Selecting Sheets '''
        logging.info('Sheets to select: {}'.format(selectSheets))
        for sheet in selectSheets:
            if sheet in available_sheets:
                filtered_sheets.append(sheet)
            else:
                logging.error(
                    '{} is not found. Please validate input \"selectSheet\".'.format(sheet))
                sys.exit(1)
    elif ignoreSheets:
        '''Ignoring Sheets'''
        logging.info('Sheets to ignore: {}'.format(ignoreSheets))
        filtered_sheets = available_sheets
        for sheet in ignoreSheets:
            if sheet in available_sheets:
                filtered_sheets.remove(sheet)
            else:
                logging.error(
                    '{} is not found. Please validate input \"ignoreSheets\".'.format(sheet))
                sys.exit(1)
    else:
        '''Do nothing'''
        filtered_sheets = available_sheets

    return filtered_sheets


def processing(in_table, destination):
    # Processing each table in the input mapping
    for table in in_table:
        logging.info("Processing: {0}".format(table))
        if addFileName:
            tablename = (table.replace(DEFAULT_FILE_INPUT, '')
                         ).replace('.xlsx', '') + '_'
        else:
            tablename = ''

        xls = pd.ExcelFile(table)
        sheet_names = xls.sheet_names
        # sheet1 = xls.parse(sheet_names[0])  # noqa
        logging.info('Available Sheets: {}'.format(sheet_names))
        sheets_to_parse = filter_sheets(
            sheet_names, selectSheets, ignoreSheets)
        logging.info("Number of pages: {0}".format(len(sheets_to_parse)))

        for sheet in sheets_to_parse:
            logging.info("Parsing {1}...".format(in_table, sheet))
            data = xls.parse(sheet)
            data.to_csv(destination+tablename+sheet+".csv", index=False)
            # produce_manifest(sheet+".csv", destination)

        logging.info("Exporting [{0}] to [{1}]".format(table, destination))

    return


def produce_manifest(file_name, destination):
    """
    Dummy function to return header per file type.
    """

    file = destination+str(file_name)+".manifest"
    # destination_part = file_name.split(".csv")[0]

    manifest_template = {  # "source": "myfile.csv"
        # ,"destination": "in.c-mybucket.table"
        # "incremental": True
        # ,"primary_key": ["VisitID","Value","MenuItem","Section"]
        # ,"columns": [""]
        # ,"delimiter": "|"
        # ,"enclosure": ""
    }

    # column_header = []

    manifest = manifest_template
    # manifest["destination"] = "in.c-hospitalityGEM."+str(destination_part)

    try:
        with open(file, 'w') as file_out:
            json.dump(manifest, file_out)
            logging.info("Output manifest file {0} produced.".format(file))
    except Exception as e:
        logging.error("Could not produce output file manifest.")
        logging.error(e)

    return


def main():
    """
    Main execution script.
    """

    # Files input
    processing(in_files, DEFAULT_FILE_DESTINATION)
    # Table input
    # sys.exit(0)
    # in_table, in_tables_names = get_tables(in_tables)
    # processing(in_table, DEFAULT_TABLE_DESTINATION)


if __name__ == "__main__":

    main()

    logging.info("Done.")
