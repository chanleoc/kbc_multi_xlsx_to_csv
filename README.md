# Processor kbc_multi_xlsx_to_csv

Taking all XLSX files from `/data/in/files` and convert selected sheets to CSV. Selected sheets will be stored with the sheet name to `/data/out/files`.

### Prerequisites
1. All files must be XLSX formatted
2. Number of sheets in XLSX cannot exceed 49

### Optional Parameters:
1. addFileName
    - `Boolean` JSON input
    - allows users to have the option to append original XLSX filename into the output sheet filename
    - Example:
    
        ```
        XLSX input: input_file.xlsx;
        Available sheet: Sheet_1
        ```
        
        ```
        addFileName: False;
        Output file >> Sheet_1.csv
        ```
        
        ```
        addFileName: True;
        Output file >> input_file_Sheet_1.csv
        ```
        
2. selectSheets
    - `List` JSON input
    - allows users to select the sheets to output
    - If both `selectSheets` and `ignoreSheets` are not configured, all sheets from XLSX will be outputted
    - If `selectSheets` is configured, it will overwrite any inputs from `ignoreSheets`
    
3. ignoreSheets
    - `List` JSON input 
    - allows users to ignore the sheets to output
    - If both `selectSheets` and `ignoreSheets` are not configured, all sheets from XLSX will be outputted
    - If `selectSheets` is configured, it will overwrite any inputs from `ignoreSheets`

    - Example
    
        ```
        XLSX input: input_file.xlsx;
        Available sheets: [Sheet_1, Sheet_2, Sheet_3]
        ```
        
        ```
        Configuration:
            {
                "selectSheets": ["Sheet_1", "Sheet_2"],
                "ignoreSheets": ["Sheet_1"]
            }
        ```
        
        ```
        Output File >>
            Sheet_1.csv,
            Sheet_2.csv
        ```

### Sample Configuration

Default Parameters
```
{
    "definition": {
        "component": "leochan.processor-break-up-xlsx-sheets"
    }
}
```

With optional parameters:
```
{
    "definition": {
        "component": "leochan.processor-break-up-xlsx-sheets"
    },
    "parameters": {
        "addFileName": True,
        "selectSheets": [
            "Sheet 1"
        ],
        "ignoreSheets" : []
    }
}
```